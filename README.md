# CSCI-759 Computations on Encrypted Data (CoED)

For more info, please visit [my teaching page](https://www.cs.rit.edu/~ph/teaching).

For RIT students, you can find more information on myCourses.

If you need to sync your forked repo with upstream repo, check [here](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/syncing-a-fork)

# Sync up to this repo

Add `upstream` repo URL to your forked repo
```bash
git remote add upstream https://gitlab.com/SpiRITlab/coed.git
```

Then if you type ```git remote -v``` if should show the following
```bash
origin	https://gitlab.com/YOUR-USERNAME/coed.git (fetch)
origin	https://gitlab.com/YOUR-USERNAME/coed.git (push)
upstream	https://gitlab.com/SpiRITlab/coed.git (fetch)
upstream	https://gitlab.com/SpiRITlab/coed.git (push)
```

Now, run this command to pull from this repo
```bash
git pull upstream master
```

Once, the pull command is done. Your repo should have my latest changes. Then push the changes to your forked repo.
```bash
git push
```

Note, you might need to resolve conflicts if there are mismatches in the two repo.


# Install compiler and other system packages 

Note, you can use either gcc-9 or gcc-10.

### Ubuntu 18.04
```bash
sudo apt install build-essential m4 libpcre3-dev gcc-9 g++-9 pkg-config
```

### Mac OSX
```bash
# Starting installation of needed dependancies and software
brew update

# install mac commandline tools
xcode-select --install

brew install gcc@9
```

# Install Homomorphic Encryption libraries: HElib and SEAL, and their dependencies
```bash
bash bootstrap.sh
```


